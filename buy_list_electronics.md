# Buy List (Electronics)

## For the Car
1. **Arduino Nano** -- controller_
2. **NRF24L01+**    -- 2.4GHz radio module
3. **L298N**        -- Dual full bridge dc-motor driver
4. **DSN-MINI-360** -- DC-DC Buck converter
5. **100uF 16V capacitor** -- _for radio module_
6. 3pcs **2N3904** -- NPN bipolar junction transistor
7. 3pcs **100Ohms resistor**
8. **10Ohms resistor**
9. **4x18650 battery holder**
10. 4pcs **18650 battery** -- _i've took it from old laptop battery_
11. **550 dc motor** -- _i've took it from the old ink-jet printer_
12. **MG90S** -- servo for steering mechanics
13. 2pcs **3mm red LED** -- for the tail lights
14. 2pcs **3mm white LED** -- for the reverse lights
15. 2pcs **5mm red LED** -- for the brake lights
16. 2pcs **5050 white LED** -- for the headlights
17. 2pcs **30 Degree Angle Lens for 5050 LEDs**
18. **SS12D00G4** -- 2 Position 3 Pin Slide Switch

Also, connectors, wires and 40x40mm breadboard

## For the RC Pad
1. **Arduino Nano** -- controller_
2. **NRF24L01+**    -- 2.4GHz radio module
3. **XY-016**       -- DC-DC Boost converter
4. 2pcs **KY-023**  -- XY-axis joystick module
5. **100uF 16V capacitor** -- _for radio module_
6. **12x12 push button**
7. **SS12D00G4** -- 2 Position 3 Pin Slide Switch
8. **3mm green LED** -- 'ready' indicator
9. **1x18650 battery holder**
10. **18650 battery**