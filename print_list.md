# Print List

## RC Pad

* 006-001_RC_Pad.stl


## Car

* 2x  001-001_Frame.stl                       ABS recommended -- due to motor can be too hot for PLA
* 2x  001-002 Battery Holder Support.stl
* 1x  001-003 Cable Holder.stl (optional)
* 1x  001-004 Underframe.stl
* 1x  002-001 Secondary Gear.stl
* 1x  002-002 Primary Gear.stl
* 4x  003-001 Rim.stl
* 4x  003-002 Tire.stl                        SEBS/Rubber material recommended
* 1x  004-001 Steering Clamp.stl
* 2x  004-002 Rod End.stl
* 2x  004-003 Lower Arm.stl
* 1+1 004-006 Steering Knuckle B.stl (_one original + one mirrored_)
* 2x  004-104 Arm Bushing.stl
* 1x  004-105 Normal enforcer.stl
* 1x  005-001 Hood C.stl
* 1x  005-002 Front Panel.stl
* 1x  005-003 Front Bumper B.stl
* 1x  005-004 Hood Holder.stl
* 1x  005-005 Front Hood.stl