/*
*  RC Car Firmware
*  Created by BP3
*  2019/02/27
*  https://gitlab.com/bp3_niva/rc-car
*/

#include "RF24.h"
#include <Servo.h>

// pins
#define PIN_SERVO       6
#define PIN_RF_CE		7
#define PIN_RF_CSN		8
#define PIN_M_SPEED     5
#define PIN_M1          A0
#define PIN_M2          A1
#define PIN_L_BRAKE     A2
#define PIN_L_BACK      A3
#define PIN_HEADLIGHTS  A4

// settings
#define PACK_SIZE		3
#define CHANNEL			5
#define PIPE			0x1234567890LL
#define CELL_ACCEL      0
#define CELL_STEER      1
#define CELL_KEYS       2
#define BTN_PAUSE       200
#define RING_TOLERANCE  2

// accelerator settings
#define ACC_NEUTRAL     485
#define ACC_TOLERANCE   3
#define MIN_SPEED       20
#define MAX_SPEED_NOTURBO 180

// steering settings
#define ST_NEUTRAL      528
#define ST_TOLERANCE    3
#define ANGLE_DEF       90
#define ANGLE_MAX       40

// break settings       DEPRECATED!
#define BRAKE_SIGNAL    HIGH
#define BRAKE_SPEED     255

// binary values
#define BTN_LIGHTS		0b00000001
#define BTN_EBRAKE		0b00000010
#define BTN_TURBO       0b00000100

// constants
#define V_BREAK         0
#define V_FORWARD       1
#define V_BACKWARD      2

// options
//#define DEBUG

RF24    radio(PIN_RF_CE, PIN_RF_CSN);
Servo   servo;
int     data[PACK_SIZE]; //accelerator, steering, keys

byte    vector = V_BREAK;
int     speed = 0;
int     servo_angle = 90;
int     lastServo = servo_angle;
bool    headlights = false;
bool    turbo = false;

bool    brakeState = false;

unsigned long lastLights = 0;
unsigned long lastTurbo = 0;

void setup()
{
    #ifdef DEBUG
    Serial.begin(9600);
    #endif

    pinMode(PIN_M_SPEED, OUTPUT);
    pinMode(PIN_M1, OUTPUT);
    pinMode(PIN_M2, OUTPUT);
    pinMode(PIN_L_BRAKE, OUTPUT);
    pinMode(PIN_L_BACK, OUTPUT);
    pinMode(PIN_HEADLIGHTS, OUTPUT);

    servo.attach(PIN_SERVO);

	radio.begin();
    radio.setChannel(CHANNEL);
    radio.setDataRate(RF24_1MBPS);
    radio.setPALevel(RF24_PA_HIGH);
    radio.openReadingPipe(1, PIPE);
    radio.startListening();
}

void loop()
{
	if (radio.available()) {
        radio.read(&data, sizeof(data));
        
        #ifdef DEBUG
        for(int i = 0; i < PACK_SIZE; i++)
        {
            Serial.print(" ");
            Serial.print(data[i]);
        }
        Serial.println();
        #endif

        speed = processAcceleratorData(data[CELL_ACCEL]);
        processSteeringData(data[CELL_STEER]);
        processKeysData(data[CELL_KEYS]);

        setServoState();
        setMotorState();
    }
}

void setServoState() {
    if (abs(lastServo - servo_angle) > RING_TOLERANCE) {
        #ifdef DEBUG
        Serial.print(" servo = ");
        Serial.print(servo_angle);
        #endif
        servo.write(servo_angle);
        lastServo = servo_angle;
    }
}

void setMotorState() {
    digitalWrite(PIN_L_BACK, vector == V_BACKWARD);
    digitalWrite(PIN_L_BRAKE, vector == V_BREAK);
    digitalWrite(PIN_HEADLIGHTS, headlights);

    if (speed < MIN_SPEED) {
        speed = 0;
    }

    #ifdef DEBUG
        Serial.print("vector = ");
        Serial.print(vector);
        Serial.print(" speed = ");
        Serial.print(speed);
        Serial.print(" servo_angle = ");
        Serial.print(servo_angle);
    #endif

    if (vector == V_FORWARD) {
        digitalWrite(PIN_M1, HIGH);
        digitalWrite(PIN_M2, LOW);
        analogWrite(PIN_M_SPEED, speed);
        return;
    }

    if (vector == V_BACKWARD) {
        digitalWrite(PIN_M1, LOW);
        digitalWrite(PIN_M2, HIGH);
        analogWrite(PIN_M_SPEED, speed);
        return;
    }

    if (vector == V_BREAK) {
        digitalWrite(PIN_M1, brakeState);
        digitalWrite(PIN_M2, !brakeState);
        analogWrite(PIN_M_SPEED, BRAKE_SPEED);
        brakeState = !brakeState;
    }
}

int processAcceleratorData(int accel) {
    if (accel <= ACC_NEUTRAL - ACC_TOLERANCE) {
        vector = V_BACKWARD;
        return map(accel, 0, ACC_NEUTRAL - ACC_TOLERANCE, turbo ? 255 : MAX_SPEED_NOTURBO, 0);
    }

    vector = V_FORWARD;

    if (accel <= ACC_NEUTRAL + ACC_TOLERANCE) {
        return 0;
    }

    if (accel > 1020) {
        return turbo ? 255 : MAX_SPEED_NOTURBO;
    }

    return map(accel, ACC_NEUTRAL + ACC_TOLERANCE, 1020, 0, turbo ? 255 : MAX_SPEED_NOTURBO);
}

void processSteeringData(int steering) {
    if (steering < ST_NEUTRAL - ST_TOLERANCE) {
        servo_angle = map(steering, 0, ST_NEUTRAL - ST_TOLERANCE, ANGLE_DEF + ANGLE_MAX, ANGLE_DEF);
        return;
    }

    if (steering > ST_NEUTRAL + ST_TOLERANCE) {
        servo_angle = map(steering, ST_NEUTRAL + ST_TOLERANCE, 1023, ANGLE_DEF, ANGLE_DEF - ANGLE_MAX);
        return;
    }

    servo_angle = ANGLE_DEF;
}

void processKeysData(int keys) {
    if ((keys & BTN_LIGHTS) && (millis() > lastLights + BTN_PAUSE)) {
        headlights = !headlights;
        lastLights = millis();
    }

    if ((keys & BTN_TURBO) && (millis() > lastTurbo + BTN_PAUSE)) {
        turbo = !turbo;
        lastTurbo = millis();
    }

    if ((keys & BTN_EBRAKE)) {
        vector = V_BREAK;
    }
}
