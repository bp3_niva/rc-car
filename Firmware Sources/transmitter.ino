/*
*  RC Pad Firmware
*  Created by BP3
*  2019/02/27
*  https://gitlab.com/bp3_niva/rc-car
*/

#include <SPI.h>
#include "RF24.h"

#define PIN_LED			4
#define PIN_RF_CE		7
#define PIN_RF_CSN		8
#define PIN_BTN_TURBO	9
#define PIN_MOSI		11
#define PIN_MISO		12
#define PIN_SCK			13
#define PIN_BTN_LIGHTS	A0
#define PIN_BTN_EBRAKE	A1
#define PIN_FWD_REV		A6
#define PIN_LEFT_RIGHT  A7

#define PACK_SIZE		3
#define CHANNEL			5
#define PIPE			0x1234567890LL

#define BTN_LIGHTS		0b00000001
#define BTN_EBRAKE		0b00000010
#define BTN_TURBO		0b00000100

//#define DEBUG

RF24 radio(PIN_RF_CE, PIN_RF_CSN);
int data[PACK_SIZE];

void setup()
{
	pinMode(PIN_BTN_LIGHTS, INPUT_PULLUP);
	pinMode(PIN_BTN_EBRAKE, INPUT_PULLUP);
	pinMode(PIN_BTN_TURBO, INPUT_PULLUP);
    pinMode(PIN_LEFT_RIGHT, INPUT);
	pinMode(PIN_FWD_REV, INPUT);

	radio.begin();
	radio.setChannel(CHANNEL);
	radio.setDataRate(RF24_1MBPS);
	radio.setPALevel(RF24_PA_HIGH);
	radio.openWritingPipe(PIPE);

	#ifdef DEBUG
	Serial.begin(9600);
	#endif

	digitalWrite(PIN_LED, HIGH);
}

void loop()
{
	data[0] = analogRead(PIN_FWD_REV);
	data[1] = analogRead(PIN_LEFT_RIGHT);
	data[2] = 0;

	if (digitalRead(PIN_BTN_EBRAKE) == LOW) {
		data[2] += BTN_EBRAKE;
	}

	if (digitalRead(PIN_BTN_LIGHTS) == LOW) {
		data[2] += BTN_LIGHTS;
	}

	if (digitalRead(PIN_BTN_TURBO) == LOW) {
		data[2] += BTN_TURBO;
	}

	sendMyData();
}

void sendMyData() {
	#ifdef DEBUG
	for(int i = 0; i < PACK_SIZE; i++)
	{
		Serial.println(data[i]);
	}
	#endif

	radio.write(&data, sizeof(data));
}
